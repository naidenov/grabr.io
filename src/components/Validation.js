import React from 'react';
import PropTypes from 'prop-types';

export default class Validation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  getVal() {
    this.validation();

    return this.state;
  }

  validation(e) {
    let value = e ? e.target.value : this.state.value;

    if (!this.props.useHTML5Validation) {
      let valid = true;

      this.props.maxLength && (!this.testMaxLength(value) && (valid = false));
      this.props.minLength && (!this.testMinLength(value) && (valid = false));
      this.props.regexp && (!this.testRegEXP(value) && (valid = false));
      this.props.required && (!this.testRequired(value) && (valid = false));

      if (this.state.valid !== valid) {
        this.state.valid = valid;

        this.setState({valid})
      }
    }

    this.state.value !== value && this.setState({value});
  }

  testRequired(value = '') {
    return !!value.length;
  }

  testRegEXP(value = '') {
    return this.props.regexp.test(value);
  }

  testMaxLength(value) {
    return this.props.maxLength >= value.length + 1;
  }

  testMinLength(value) {
    return this.props.minLength <= value.length + 1;
  }

  render() {
    let html5ValidationRules = {};

    if (this.props.useHTML5Validation) {
      html5ValidationRules = {
        required: this.props.required ? true : void 0,
        min:      this.props.minLength ? this.props.minLength : void 0,
        max:      this.props.maxLength ? this.props.maxLength : void 0,
        pattern:  this.props.regexp ? this.props.regexp : void 0
      }
    }

    return (<div className={this.state.valid ? '' : 'validation-error'}>
        {React.cloneElement(this.props.children, {
          onChange: this.validation.bind(this),
          value:    this.state.value,
          ...html5ValidationRules
        })}
      </div>
    )
  }
}

Validation.propTypes    = {
  useHTML5Validation: PropTypes.bool
};
Validation.defaultProps = {
  useHTML5Validation: false
};