import React from 'react';
import Validation from './Validation';
import {connect} from "react-redux";
import {addUser, updateUser} from './../actions';
import {Link, Redirect} from "react-router-dom";
import DateSelect from './DateSelect';

const mapDispatchToProps = (dispatch) => {
  return {
    add:    user => dispatch(addUser(user)),
    update: (user, i) => dispatch(updateUser(user, i))
  }
};

const mapStateToProps = state => {
  return {users: state.users};
};

class UserForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      needRedirect:                   false,
      saved:                          this.props.location.state ? !!this.props.location.state.saved : false,
      updated:                        false,
      index:                          +props.match.params.id,
      showFullNameValidationError:    false,
      showCityValidationError:        false,
      showAddressValidationError:     false,
      showPhoneNumberValidationError: false,
      user:                           !props.match.params.id ?
                                        void 0 :
                                        this.props.users.filter((u, i) => i == props.match.params.id)[0]
    }
  }

  componentWillUnmount() {
    if (this.timer)
      clearTimeout(this.timer);
  }

  submitForm(event) {
    event.preventDefault();

    const fullName    = this.fullName.getVal();
    const birthday    = this.birthday.getVal();
    const city        = this.city.getVal();
    const address     = this.address.getVal();
    const phoneNumber = this.phoneNumber.getVal();

    this.setState({
      showFullNameValidationError:    !fullName.valid,
      showCityValidationError:        !city.valid,
      showAddressValidationError:     !address.valid,
      showPhoneNumberValidationError: !phoneNumber.valid
    });

    if (fullName.valid && city.valid && address.valid && phoneNumber.valid) {
      let user = {
        full_name:    fullName.value,
        birthday:     birthday.value,
        address:      address.value,
        city:         city.value,
        phone_number: phoneNumber.value
      };

      if (this.state.user) {
        this.props.update(user, this.state.index);
        this.setState({
          updated: true,
          saved:   false
        });
      } else {
        this.props.add(user);
        this.setState({
          needRedirect: true
        });
      }
    }
  }

  setupAlertTimer() {
    if (this.state.saved || this.state.updated) {
      if (!this.timer)
        this.timer = setTimeout(() => {
          this.setState({
            saved:   false,
            updated: false
          });

          clearTimeout(this.timer);
          this.timer = void 0;
        }, 3000);
    }
  }

  render() {
    // this.setupAlertTimer();

    if (this.state.needRedirect)
      return <Redirect to={{
        pathname: "/user-0",
        state:    {saved: true}
      }}/>;

    return (
      <form onSubmit={this.submitForm.bind(this)}>
        <nav className="navbar navbar-expand-md navbar-light bg-light">
          <Link to="/">Пользователи</Link>
        </nav>

        {this.state.saved && <div className="alert alert-success fade show" color="success">Пользователь создан</div>}
        {this.state.updated &&
        <div className="alert alert-primary fade show" color="success">Пользователь обновлён</div>}

        {this.state.showFullNameValidationError &&
        <div className="alert alert-danger fade show" color="success">Неправильно заполнено поле ФИО</div>}
        {this.state.showCityValidationError &&
        <div className="alert alert-danger fade show" color="success">Неправильно заполнено поле Город</div>}
        {this.state.showAddressValidationError &&
        <div className="alert alert-danger fade show" color="success">Неправильно заполнено поле Адрес</div>}
        {this.state.showPhoneNumberValidationError &&
        <div className="alert alert-danger fade show" color="success">Неправильно заполнено поле Номер Телефона</div>}

        <div>
          <div className="position-relative form-group">
            <label>Ф.И.О.</label>
            <Validation
              useHTML5Validation
              maxLength={100}
              required
              ref={(elem) => {
                this.fullName = elem;
              }}
              value={this.state.user ? this.state.user.full_name : ''}
            >
              <input placeholder="Иванов Иван Иванович" className="form-control"></input>
            </Validation>
          </div>

          <div className="position-relative form-group">
            <label>Дата Рождения</label>
            <Validation
              required
              ref={(elem) => {
                this.birthday = elem;
              }}
              value={this.state.user ? this.state.user.birthday : ''}
            >
              <DateSelect/>
            </Validation>
          </div>

          <div className="position-relative form-group">
            <label>Город</label>
            <Validation
              required
              value={this.state.user ? this.state.user.city : ''}
              ref={(elem) => {
                this.city = elem;
              }}>
              <input placeholder="Москва" className="form-control"></input>
            </Validation>
          </div>

          <div className="position-relative form-group">
            <label>Адрес</label>
            <Validation
              required
              maxLength={255}
              ref={(elem) => {
                this.address = elem;
              }}
              value={this.state.user ? this.state.user.address : ''}
            >
              <input placeholder="ул. Фрунзе 12 - 45" className="form-control"></input>
            </Validation>
          </div>

          <div className="position-relative form-group">
            <label>Телефон</label>
            <Validation
              regexp={/^\+7/}
              maxLength={13}
              minLength={13}
              required
              ref={(elem) => {
                this.phoneNumber = elem;
              }}
              value={this.state.user ? this.state.user.phone_number : ''}
            >
              <input placeholder="+7" className="form-control"></input>
            </Validation>
          </div>

          <button className="btn btn-secondary"
                  >{this.state.user ? 'Обновить' : 'Сохранить'}</button>
        </div>
      </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
