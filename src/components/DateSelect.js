import React from 'react';
import moment from 'moment';

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: moment(this.props.value ? this.props.value : void 0).format('YYYY-MM-DD'),
      month: moment(this.props.value ? this.props.value : void 0).format('MM'),
      day:   moment(this.props.value ? this.props.value : void 0).format('DD'),
      year:  moment(this.props.value ? this.props.value : void 0).format('Y')
    };
  }

  getVal() {
    return `${this.state.year}-${this.state.month}-${this.state.day}`;
  }

  getYearList() {
    let y   = +moment().format('YYYY');
    let res = [];

    while (y > 1950) {
      res.push(y);
      y--;
    }

    return res;
  }

  getDayList() {
    let daysInMonth = +moment(`${this.state.year}-${this.state.month}`, "YYYY-MM").daysInMonth();
    let res         = [];
    let d           = 1;

    while (d <= daysInMonth) {
      res.push(d < 10 ? `0${d}` : d);
      d++;
    }

    return res;
  }

  getMonthList() {
    let m   = 1;
    let res = [];

    while (m <= 12) {
      res.push(m < 10 ? `0${m}` : m);
      m++;
    }

    return res;
  }

  changeYear(e) {
    this.setState({
      year: e.target.value
    }, () => {
      this.props.onChange({target: {value: this.getVal()}})
    });
  }

  changeMonth(e) {
    this.setState({
      month: e.target.value
    }, () => {
      this.props.onChange({target: {value: this.getVal()}})
    });
    ;
  }

  changeDay(e) {
    this.setState({
      day: e.target.value
    }, () => {
      this.props.onChange({target: {value: this.getVal()}})
    });
  }

  render() {
    return (
      <div className="row">
        <div className="col-6 col-sm-4">
          <label>День</label>
          <select
            className="form-control"
            defaultValue={this.state.day}
            onChange={this.changeDay.bind(this)}
          >
            {this.getDayList().map((d, i) => {
              return <option key={i} value={d}>{d}</option>
            })}
          </select>
        </div>
        <div className="col-6 col-sm-4">
          <label>Месяц</label>
          <select
            className="form-control"
            defaultValue={this.state.month}
            onChange={this.changeMonth.bind(this)}
          >
            {this.getMonthList().map((m, i) => {
              return <option key={i} value={m}>{m}</option>
            })}
          </select>
        </div>
        <div className="col-sm-4">
          <label>Год</label>
          <select
            className="form-control"
            defaultValue={this.state.year}
            onChange={this.changeYear.bind(this)}
          >
            {this.getYearList().map((y, i) => {
              return <option key={i} value={y}>{y}</option>
            })}
          </select>
        </div>
      </div>
    )
  }
}