import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";
import store from "./store/index";
import {APP_USERS_LOACAL_STORAGE_KEY} from "./constants/action-types";

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import App from "./components/App";

store.subscribe(() => localStorage.setItem(APP_USERS_LOACAL_STORAGE_KEY, JSON.stringify(store.getState().users)));

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);