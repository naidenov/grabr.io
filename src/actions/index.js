import {ADD_USER, REMOVE_USER, UPDATE_USER} from "../constants/action-types";

export const addUser    = userData => ({type: ADD_USER, payload: {user: userData}});
export const updateUser = (userData, index) => ({type: UPDATE_USER, payload: {user: userData, index}});
export const removeUser = index => ({type: REMOVE_USER, payload: {index}});