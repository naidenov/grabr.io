import React from "react";
import {connect} from "react-redux";
import {removeUser} from './../actions';
import {Link} from "react-router-dom";

const mapStateToProps = state => {
  return {users: state.users};
};

const mapDispatchToProps = dispatch => {
  return {
    remove: index => dispatch(removeUser(index))
  };
};

const UserList = ({users, remove}) => {
  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-light bg-light">
        <Link to="/create">Добавить пользователя</Link>
      </nav>

      <table className="table">
        <thead>
        <tr>
          <th>#</th>
          <th>Ф.И.О.</th>
          <th>Город</th>
          <th>Номер телефона</th>
          <th>Удалить</th>
        </tr>
        </thead>
        <tbody>
        {users.map((user, inedx) => (
          <tr key={inedx}>
            <th scope="row">{inedx + 1}</th>
            <td>
              <Link to={`/user-${inedx}`}>{user.full_name}</Link>
            </td>
            <td>{user.city}</td>
            <td>{user.phone_number}</td>
            <td>
              <button type="button" className="btn btn-danger" color="danger" onClick={remove.bind(null, inedx)}>Удалить
              </button>
            </td>
          </tr>
        ))}
        </tbody>
      </table>
    </div>
  )
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);