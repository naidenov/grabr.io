import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import List from './List';
import UserForm from './UserForm';

const App = () => (
  <Router>
    <div className="container">
      <div>
        <Route exact path="/create" component={UserForm}/>
        <Route path="/user-:id" component={UserForm}/>
        <Route exact path="/" component={List}/>
      </div>
    </div>
  </Router>
);

export default App;