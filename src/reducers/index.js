import {ADD_USER, REMOVE_USER, UPDATE_USER, APP_USERS_LOACAL_STORAGE_KEY} from "../constants/action-types";

const data     = localStorage.getItem(APP_USERS_LOACAL_STORAGE_KEY);
const testData = [{
  full_name:    'Пертренко Пётр Иванович',
  birthday:     '1988-04-22',
  address:      'ул. Фрунзе 12',
  city:         'г. Москва',
  phone_number: '+79236272832'
}];

const initialState = {
  users: data ? JSON.parse(data) : testData
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER:
      return {...state, users: [action.payload.user, ...state.users]};
    case REMOVE_USER:
      return {...state, users: state.users.filter((u, i) => i !== action.payload.index)};
    case UPDATE_USER:
      return {...state, users: state.users.map((u, i) => i !== action.payload.index ? u : action.payload.user)};
    default:
      return state;
  }
};

export default rootReducer;